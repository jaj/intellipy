from setuptools import setup

setup(name = 'intellipy',
      version = '0.0.1',
      description = 'IntelliPy',
      url = 'https://github.com/jaj42/intellipy',
      author = 'Jona JOACHIM',
      author_email = 'jona@joachim.cc',
      license = 'MIT',
      python_requires = '>=3.4',
      install_requires = [],
      scripts = [],
      packages = ['intellipy', 'intellipy/IntellivueDataFiles', 'intellipy/Sockets'],
      include_package_data = True
)

#dep: pyserial, numpy
